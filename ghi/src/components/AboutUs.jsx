import { Typography, Container, Box, IconButton } from '@mui/material'
import logo from '../assets/logo.png'

export default function AboutUs() {
    return (
        <Container>
            <Box
                component="img"
                my={25}
                align="left"
                p={9}
                sx={{
                    width: 450,
                    height: 300,
                }}
                src={logo}
                alt="Logo"
            />
            <IconButton size="large" color="inherit"></IconButton>
            <Typography
                variant="h3"
                color="textSecondary"
                height={700}
                width={900}
                align="left"
                p={26}
            >
                ZenDen is a blogsite where you can post blogs about all things
                related to Serenity. You can also interact with other users by
                posting comments.
            </Typography>
        </Container>
    )
}
