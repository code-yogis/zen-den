import { useState, useEffect } from 'react'
import {
    Container,
    Box,
    Typography,
    Card,
    CardMedia,
    CardContent,
    CircularProgress,
} from '@mui/material'

function HomePage() {
    const [blogs, setBlogs] = useState([])
    const [currentBlogIndex, setCurrentBlogIndex] = useState(0)
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        const fetchBlogs = async () => {
            const response = await fetch(
                `${import.meta.env.VITE_API_HOST}/blogs/`
            )
            if (response.ok) {
                const data = await response.json()
                setBlogs(data)
                setTimeout(() => {
                    setLoading(false)
                }, 500)
            }
        }

        fetchBlogs()
    }, [])

    useEffect(() => {
        const intervalId = setInterval(() => {
            setCurrentBlogIndex((prevIndex) => (prevIndex + 1) % blogs.length)
        }, 10000) // Change every 10 seconds

        return () => clearInterval(intervalId) // Clean up the interval on component unmount
    }, [blogs.length])

    return (
        <Container sx={{ maxWidth: '100vw', padding: '20px' }}>
            <Box
                my={12}
                sx={{ width: '100%', textAlign: 'center' }}
                textAlign="center"
            >
                {loading ? (
                    <CircularProgress />
                ) : (
                    blogs.length > 0 && (
                        <Card sx={{ maxWidth: '100%', margin: '20px' }}>
                            <CardMedia
                                component="img"
                                image={
                                    blogs[currentBlogIndex]?.pic_url ||
                                    'https://images.pexels.com/photos/6913382/pexels-photo-6913382.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2'
                                }
                                alt={blogs[currentBlogIndex]?.title}
                            />
                            <CardContent>
                                <Typography variant="h5" component="div">
                                    {blogs[currentBlogIndex]?.title}
                                </Typography>
                                <Typography
                                    variant="body2"
                                    color="textSecondary"
                                >
                                    {blogs[currentBlogIndex]?.user.username}
                                </Typography>
                                <Typography
                                    variant="body2"
                                    color="textSecondary"
                                >
                                    {blogs[currentBlogIndex]?.date_published}
                                </Typography>
                            </CardContent>
                        </Card>
                    )
                )}
            </Box>
        </Container>
    )
}

export default HomePage
