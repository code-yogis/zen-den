import { AppBar, Toolbar, Stack, Typography, Button } from '@mui/material'
import { useNavigate } from 'react-router-dom'

export default function Footer() {
    const navigate = useNavigate()
    return (
        <AppBar position="static">
            <Toolbar>
                <Typography
                    variant="h6"
                    component="div"
                    sx={{ flexGrow: 1 }}
                ></Typography>
                <Stack direction="row">
                    <Button
                        onClick={() => {
                            navigate('/aboutus')
                        }}
                        color="inherit"
                    >
                        {' '}
                        About Us{' '}
                    </Button>
                    <Button
                        onClick={() => {
                            navigate('/contactme')
                        }}
                        color="inherit"
                    >
                        {' '}
                        Contact Us{' '}
                    </Button>
                </Stack>
            </Toolbar>
        </AppBar>
    )
}
