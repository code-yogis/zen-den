steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE users (
            user_id SERIAL PRIMARY KEY NOT NULL,
            first_name VARCHAR(50) NOT NULL,
            last_name VARCHAR(100) NOT NULL,
            username VARCHAR(100) NOT NULL UNIQUE,
            password VARCHAR(256) NOT NULL,
            email VARCHAR(100) NOT NULL UNIQUE
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE users;
        """,
    ],
    [
        # "Up" SQL statement
        """
        INSERT INTO users (first_name, last_name, username, password, email) VALUES
            ('John', 'Doe', 'johndoe', 'hashed_password_1', 'johndoe@example.com'),
            ('Jane', 'Smith', 'janesmith', 'hashed_password_2', 'janesmith@example.com'),
            ('Alice', 'Johnson', 'alicejohnson', 'hashed_password_3', 'alicejohnson@example.com'),
            ('Bob', 'Brown', 'bobbrown', 'hashed_password_4', 'bobbrown@example.com');
        """,
        # "Down" SQL statement
        """
        DROP TABLE users;
        """,
    ],
]
