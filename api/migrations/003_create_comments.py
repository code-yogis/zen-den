steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE comments (
            comment_id SERIAL PRIMARY KEY,
            body VARCHAR(1500) NOT NULL,
            blog_id INT NOT NULL REFERENCES blogs (blog_id) ON DELETE CASCADE,
            author_id INT NOT NULL REFERENCES users (user_id) ON DELETE CASCADE,
            date_published DATE NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE comments;
        """,
    ],
    [
        # "Up" SQL statement
        """
        INSERT INTO comments (body, blog_id, author_id, date_published) VALUES
            ('I found these yoga poses incredibly relaxing.', 1, 1, '2024-07-05'),
            ('These fitness tips have really boosted my energy levels!', 1, 2, '2024-07-06'),
            ('Yoga helps me maintain balance in my hectic life.', 2, 1, '2024-07-07'),
            ('I love how yoga clears my mind after a stressful day.', 2, 2, '2024-07-08'),
            ('These yoga routines have improved my flexibility so much!', 3, 3, '2024-07-09'),
            ('Fitness is not just about strength; its about feeling good.', 3, 4, '2024-07-10'),
            ('I started yoga recently, and it has become my daily ritual.', 4, 3, '2024-07-11'),
            ('The benefits of yoga are more than just physical.', 4, 4, '2024-07-12'),
            ('These yoga poses have helped me relieve back pain.', 1, 3, '2024-07-13'),
            ('Fitness is a journey, and Im loving every step of it.', 1, 4, '2024-07-14'),
            ('Yoga is my escape from the chaos of daily life.', 2, 3, '2024-07-15'),
            ('These fitness challenges have motivated me to push harder.', 2, 4, '2024-07-16'),
            ('I never thought I could enjoy exercise until I found yoga.', 3, 1, '2024-07-17'),
            ('The peace I find in yoga is unparalleled.', 3, 2, '2024-07-18'),
            ('Fitness is about feeling strong and confident in my own skin.', 4, 1, '2024-07-19'),
            ('Yoga has taught me patience and perseverance.', 4, 2, '2024-07-20');
        """,
        # "Down" SQL statement
        """
        DROP TABLE comments;
        """,
    ],
]
