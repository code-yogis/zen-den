steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE blogs (
        blog_id SERIAL PRIMARY KEY,
        title VARCHAR(200) NOT NULL,
        pic_url VARCHAR(1500) NOT NULL,
        content TEXT NOT NULL,
        author_id SMALLINT NOT NULL REFERENCES users (user_id) ON DELETE CASCADE,
        date_published DATE NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE blogs;
        """,
    ],
    [
        # "Up" SQL statement
        """
        INSERT INTO blogs (title, pic_url, content, author_id, date_published) VALUES
            ('Yoga for Beginners', 'https://www.thegoodbody.com/wp-content/uploads/2022/11/yoga-poses-for-beginners-1200x600.png', 'A guide to starting your yoga journey.', 1, '2024-05-29'),
            ('Benefits of Yoga', 'https://ekamyogaacademy.com/public/uploads/blog/1683551811.jpg', 'Explore the numerous benefits of practicing yoga regularly.', 2, '2024-06-29'),
            ('Advanced Yoga Techniques', 'https://ekamyogaacademy.com/public/uploads/blog/1680693345.jpg', 'Deep dive into advanced yoga poses and practices.', 3, '2024-07-01'),
            ('Yoga and Mental Health', 'https://res.cloudinary.com/jerrick/image/upload/c_scale,f_jpg,q_auto/64bd4f71372f94001e323e40.png', 'How yoga can improve your mental well-being.', 4, '2024-07-02');
        """,
        # "Down" SQL statement
        """
        DROP TABLE blogs;
        """,
    ],
]
